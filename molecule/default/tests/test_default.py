import os

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_haproxy_config_file(host):
    f = host.file('/etc/haproxy/haproxy.cfg')
    assert f.exists


def test_haproxy_running(host):
    haproxy_svc = host.service("haproxy")
    haproxy_svc.is_running


def test_haproxy_connection(host):
    assert host.socket("tcp://127.0.0.1:81").is_listening
